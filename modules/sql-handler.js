const mysql = require('mysql');
const sqlAuth = require('../sqlAuth.json');

const trace = require('./trace.js');
var TRACE = trace.TRACE;


var db_config={
	connectionLimit : 100, //important
	host: sqlAuth.host,
	user: sqlAuth.user,
	password: sqlAuth.password,
	database: sqlAuth.database,
	port: sqlAuth.port,
	debug: false
};

var sqlCon = mysql.createPool(db_config);

query("CREATE TABLE IF NOT EXISTS tokens (TokenID int(11) NOT NULL AUTO_INCREMENT, Token text NOT NULL, RefreshToken text NOT NULL, CharacterID int(11) NOT NULL, CharacterName text NOT NULL, CorporationID bigint(20) DEFAULT NULL, CorporationName text, CharacterOwnerHash text NOT NULL, ExpiresOn datetime NOT NULL, IntellectualProperty text NOT NULL, Scopes text NOT NULL, TokenType text NOT NULL, isNew tinyint(1) NOT NULL DEFAULT '1', PRIMARY KEY (TokenID), UNIQUE KEY TokenID (TokenID), KEY TokenID_2 (TokenID))");

module.exports = {
	sqlCon,
	query,
	setTokenUsed
};

async function query(query, params) {
	return new Promise((resolve, reject) => {
			// The second param is allowed to be null here
			sqlCon.query(query, params, async function(err, result, fields) {
				if (err) {
					TRACE("Encountered an error while running a query:\n" + 
							"Query: " + query +
							"Error: " + err);
				}
				resolve(result);
			});
		}
	);
}

async function setTokenUsed(token) {
	return new Promise(
		async function(resolve, reject) {
			var updateQuery = "UPDATE tokens SET isNew = FALSE where TokenID="+token.TokenID+";";
			result = (await query(updateQuery));
			resolve(result);
		}
	);
}

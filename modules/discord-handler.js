const Discord = require('discord.io');
const moment = require('moment');
const globe = require('./globals.js');
const sql = require('./sql-handler.js');
const discordAuth = require('../discordAuth.json');

const trace = require('./trace.js');
var TRACE = trace.TRACE;

const bot = new Discord.Client({
   token: discordAuth.token,
   autorun: true
});

const pingChannel=BigInt(discordAuth.pingChannel);

bot.on("disconnect", async function() {
	TRACE("Discord disconnected");
	await globe.sleep(10000);
	bot.connect() //Auto reconnect
});

module.exports = {
	bot,
	pingChannel
};

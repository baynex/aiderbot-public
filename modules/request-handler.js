const esi = require('eve-swagger');
const moment = require('moment');
const request = require('request');
const sql = require('./sql-handler.js');
const eve = require('../eve.json');

const trace = require('./trace.js');
var TRACE = trace.TRACE;

module.exports = {
	get,
	refresh,
	refreshTokenIfNeeded
};

sql.query("CREATE TABLE IF NOT EXISTS `tokens` (`TokenID` int(11) NOT NULL AUTO_INCREMENT,`Token` text NOT NULL,`RefreshToken` text NOT NULL,`CharacterID` int(11) NOT NULL,`CharacterName` text NOT NULL,`CorporationID` bigint(20) DEFAULT NULL,`CorporationName` text,`CharacterOwnerHash` text NOT NULL,`ExpiresOn` datetime NOT NULL,`IntellectualProperty` text NOT NULL,`Scopes` text NOT NULL,`TokenType` text NOT NULL,`isNew` tinyint(1) NOT NULL DEFAULT '1',PRIMARY KEY (`TokenID`),UNIQUE KEY `TokenID` (`TokenID`),KEY `TokenID_2` (`TokenID`))");

async function get(qObj) {
	return new Promise((resolve, reject) => {
			let headers = {
				'accept' : 'application/json'
			};
			if (qObj.token) {
				headers["Authorization"] = "Bearer " + qObj.token;
			}

			request.get({
				headers: headers,
				url: qObj.url
			}, function(error, response, body) {
				if (error) {
					TRACE("Encountered an error while trying to reach an ESI endpoint:" +
							"\nURL: " + qObj.query +
							"\nError: " + error);
				}
				resolve(body);
			});
		}
	);
}

async function refresh(token) {
	return new Promise((resolve, reject) => {
			clientID = eve.clientID;
			secret = eve.secret;
			auth = Buffer.from(clientID+":"+secret).toString('base64');

			const data = "grant_type=refresh_token&refresh_token="+token;

			request.post({
				headers: {
					'Authorization': 'Basic '+auth,
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': data.length
				},
				url: 'https://login.eveonline.com/oauth/token',
				body: data
			}, function(error, response, body){
				resolve(JSON.parse(body).access_token);
			});
		}
	);
}

async function refreshTokenIfNeeded(token) {
	//TRACE("RefreshTokenIfNeeded()+");
	//TRACE("token:  "+token.CharacterName +" - "+token.Scopes+" - "+token.ExpiresOn);
	return new Promise(
		async function(resolve, reject) {
			if(moment() >= moment(token.ExpiresOn, "YYYY-MM-DD HH:mm:ss")) {
				newToken = await refresh(token.RefreshToken)
				newExpire = moment().add(20, "minutes").format("YYYY/MM/DD HH:mm:ss");
				var updateQuery = "UPDATE tokens SET Token=\""+newToken+"\", ExpiresOn=\""+ newExpire +"\" where TokenID="+token.TokenID+";";
				result = (await sql.query(updateQuery));
				token.Token = newToken;
				token.ExpiresOn = newExpire;
			}
			resolve(token);
		}
	);
}

const moment = require('moment');

module.exports = {
	TRACE
};

function TRACE(str) {
	console.log(moment().format("MM/DD/YY kk:mm")+" - "+str);
}

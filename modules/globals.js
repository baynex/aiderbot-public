const moment = require('moment');
const sql = require('./sql-handler.js');
const req = require ('./request-handler.js');

const trace = require('./trace.js');
var TRACE = trace.TRACE;

const esiUrl = 'https://esi.evetech.net';

function sleep(ms){
    return new Promise(resolve => {
        setTimeout(resolve,ms)
    })
}

function isIterable(obj) {
  // checks for null and undefined
  if (obj == null) {
    return false;
  }
  return typeof obj[Symbol.iterator] === 'function';
}

function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function sqlFormatDateTime(str){
	return str.replace('T', ' ').replace('Z', '');
}

async function fetchStructureInfoIfNeeded(structureID, structureToken, force) {
	//TRACE("fetchStructureInfoIfNeeded()+ force:"+force);
	var checkExist = "select * from eveonline_evestructures where structure_id="+structureID;
	notexist = (await sql.query(checkExist)).length == 0;

	if(notexist) {
		structure = JSON.parse(await req.get({token:structureToken, url: esiUrl+"/latest/universe/structures/"+ structureID +"/?datasource=tranquility"}));
		//TRACE(JSON.stringify(structure) +"\n");

		if (structure['error']) return;

		var insertQuery = "INSERT INTO eveonline_evestructures (structure_id, ";

		var values = [structureID];
		for (const key in structure) {
			if (key == 'position') continue;
			insertQuery += key+", ";
			values.push(structure[key]);
		}
		insertQuery = insertQuery.slice(0, -2);
		insertQuery += ") VALUES (?)";
		var result = (await sql.query(insertQuery, [values]));

	} else if (force) {
		structure = JSON.parse(await req.get({token:structureToken, url: esiUrl+"/latest/universe/structures/"+ structureID +"/?datasource=tranquility"}));
		//TRACE(JSON.stringify(structure) +"\n");

		if (structure['error']) return;

		var updateQuery = "update eveonline_evestructures set name="+ sql.sqlCon.escape(structure['name'])+ ", owner_id="+structure['owner_id']+" where structure_id="+structureID;

		var result = (await sql.query(updateQuery));
	}
}

module.exports = {
	esiUrl,
	sleep,
	isIterable,
	formatNumber,
	sqlFormatDateTime,
	fetchStructureInfoIfNeeded
};

const moment = require('moment');
const globe = require('../modules/globals.js');
const sql = require('../modules/sql-handler.js');
const discord = require('../modules/discord-handler.js');
const req = require('../modules/request-handler.js');

const trace = require('../modules/trace.js');
var TRACE = trace.TRACE;
var firstConnect = true;

discord.bot.on('ready', async function() {
	if (firstConnect) {

		await sql.query("CREATE TABLE IF NOT EXISTS eveonline_eveplanets (planet_id int(11) NOT NULL, name tinytext NOT NULL, system_id int(11) NOT NULL, type_id int(11) NOT NULL)");

		await sql.query("CREATE TABLE IF NOT EXISTS eveonline_notifications (notification_id bigint(20) NOT NULL, is_read tinyint(1) DEFAULT NULL, sender_id int(11) NOT NULL, sender_type tinytext NOT NULL, text text, timestamp datetime NOT NULL, type tinytext NOT NULL, botPinged tinyint(1) NOT NULL DEFAULT '0', fromCorporationName tinytext NOT NULL, fromCorporationID int(11) NOT NULL, fromCharacterName text NOT NULL, fromCharacterID int(11) NOT NULL, PRIMARY KEY (notification_id), UNIQUE KEY notification_id (notification_id), KEY notification_id_2 (notification_id))");

		setInterval(pullNotificationsData, 300000);
		await pullNotificationsData();

		firstConnect = false;
	}
});

module.exports = {}

// Intervaled
async function pullNotificationsData() {
	TRACE("pullNotificationsData()+");
	notificationsTokens = (await sql.query("select * from tokens where Scopes=\"esi-characters.read_notifications.v1\""));
	for (var i = 0; i < notificationsTokens.length; i++) {
		var usableNotificationToken = await req.refreshTokenIfNeeded(notificationsTokens[i]);
		//TRACE(JSON.stringify(usableNotificationToken));
		var resp = await req.get({token:usableNotificationToken.Token, url: globe.esiUrl+"/latest/characters/"+ usableNotificationToken.CharacterID +"/notifications/?datasource=tranquility"});
		resp = JSON.parse(resp);

		for (var j = 0; j < resp.length; j++) {
			//TRACE(JSON.stringify(resp[j]));
			var checkExist = "select * from eveonline_notifications where notification_id="+resp[j]['notification_id'];
			exist = (await sql.query(checkExist)).length > 0;

			if (!exist) {
				//Insert
				var insertQuery = "INSERT INTO eveonline_notifications (fromCorporationID, fromCorporationName, fromCharacterID, fromCharacterName, ";
				var values = [usableNotificationToken.CorporationID, usableNotificationToken.CorporationName, usableNotificationToken.CharacterID, usableNotificationToken.CharacterName];
				for (const key in resp[j]) {
					insertQuery += key+", ";

					switch (key) {
						case 'timestamp':
							values.push(globe.sqlFormatDateTime(resp[j][key]));
							break;
						default:
							values.push(resp[j][key]);
							break;
					}
				}
				insertQuery=insertQuery.slice(0, -2);
				insertQuery +=") VALUES (?)";
				var result = (await sql.query(insertQuery, [values]));
				//TRACE(result);
			} else {
				//Update
				var updateQuery = "UPDATE eveonline_notifications SET ";
				var values = [];
				for (const key in resp[j]) {
					if (key == 'notification_id') continue;

					updateQuery += key+"=? , ";

					switch (key) {
						case 'timestamp':
							values.push(globe.sqlFormatDateTime(resp[j][key]) );
							break;
						default:
							values.push(resp[j][key]);
							break;
					}
				}
				updateQuery=updateQuery.slice(0, -2);
				updateQuery +=" WHERE notification_id="+resp[j]['notification_id'];
				var result = (await sql.query(updateQuery, values));
				//TRACE(result);
			}
		}
	}

	await pingNotifications();
}

async function pingNotifications() {
	TRACE("  pingNotifications()+");
	var sqlQuery = "SELECT * FROM `eveonline_notifications` where ( type='StructureUnderAttack' OR type='StructureOnline' OR type='StructureLostShields' OR type='StructureLostArmor' OR type='StructureDestroyed' OR type='StructureWentHighPower' OR type='StructureWentLowPower' OR type='OrbitalAttacked' ) and botPinged=false";
	toPing = (await sql.query(sqlQuery));

	for (var i = 0; i < toPing.length; i++) {
		TRACE(JSON.stringify(toPing[i]));
		TRACE(toPing[i].fromCorporationID);
		switch(toPing[i].type) {
				case 'StructureDestroyed':
				case 'StructureUnderAttack':
				case 'StructureOnline':
				case 'StructureLostShields':
				case 'StructureLostArmor':
				case 'StructureWentHighPower':
				case 'StructureWentLowPower':
					await sendStructureNotification(discord.pingChannel, toPing[i].type, toPing[i].text, toPing[i].timestamp);
					break;
				case 'OrbitalAttacked':
					await sendPOCONotification(discord.pingChannel, toPing[i].type, toPing[i].text, toPing[i].timestamp);
					break;
		}
		await sql.query("UPDATE eveonline_notifications SET botPinged=1 WHERE notification_id="+toPing[i].notification_id+";");
	}
}

async function sendStructureNotification(pingChannel, type, text, timestamp) {
	var arr = text.split("\n");
	var notificationData = {};
	for (var j=0;j<arr.length;j++) {
		var arr2 = arr[j].split(": ");
		switch (arr2[0]) {
			case "shieldPercentage":
				notificationData.shield = arr2[1];
				break;
			case "armorPercentage":
				notificationData.armor = arr2[1];
				break;
			case "hullPercentage":
				notificationData.hull = arr2[1];
				break;
			case "corpName":
				notificationData.corp = arr2[1];
				break;
			case "allianceName":
				notificationData.alliance = (arr2[1] == undefined ? "-" : arr2[1]);
				break;
			case "structureID":
				notificationData.structureID = arr2[1].split(" ")[1];
				var query = "SELECT name,type_id FROM eveonline_evestructures where structure_id="+notificationData.structureID;
				var structureFromSQL = (await sql.query(query));
				if (structureFromSQL.length === 0 ) {
					notificationData.structure = "Unknown Structure (ID:"+arr2[1].split(" ")[1]+")"
					notificationData.structure_type_id = "0";
				} else {
					notificationData.structure = structureFromSQL[0].name;
					notificationData.structure_type_id = structureFromSQL[0].type_id;
				}
				break;
		}
	}
	var titleString="";
	var msgString="";
	switch (type) {
				case 'StructureUnderAttack':
					titleString = notificationData.structure+" is under attack!";
					msgString = "Aggressor Corp     : "+notificationData.corp+"\n"+
								"Aggressor Alliance : "+notificationData.alliance+"\n\n"+
								"Shield : "+Math.floor(notificationData.shield)+"%\n"+
								"Armor  : "+Math.floor(notificationData.armor)+"%\n"+
								"Hull   : "+Math.floor(notificationData.hull)+"%";
					break;
				case 'StructureOnline':
					titleString = notificationData.structure+" is now online!";
					msgString = ":parrot~1:";
					break;
				case 'StructureLostShields':
					titleString = notificationData.structure+" has lost its shields!";
					msgString = ":HTFU:";
					break;
				case 'StructureLostArmor':
					titleString = notificationData.structure+" has lost its armor!";
					msgString = ":HTFU:";
					break;
				case 'StructureDestroyed':
					titleString = notificationData.structure+" has been destroyed!";
					msgString = ":neutral_face:";
					var deleteQuery = "DELETE FROM eveonline_evecorpstructures WHERE structure_id="+notificationData.structureID;
					await sql.query(deleteQuery);
					break;
				case 'StructureWentHighPower':
					titleString = "Structure State Change";
					msgString = notificationData.structure+" is now in high power mode!";
					break;
				case 'StructureWentLowPower':
					titleString = "Structure State Change";
					msgString = notificationData.structure+" is now in low power mode!";
					break;
	}

	var pingStr = "";
	if (notificationData.corp != "Guristas") {
		if (type == 'StructureUnderAttack') {
			pingStr = "@everyone";
		} else {
			pingStr = "@here";
		}
	}

	discord.bot.sendMessage({
		to: pingChannel,
		message: pingStr, // You can also send a message with the embed.
		embed: {
			description: msgString,
			color: 53424,
			timestamp: moment(timestamp).format(),
			thumbnail: {
				url: "https://imageserver.eveonline.com/Render/"+notificationData.structure_type_id+"_128.png"
			},
			title: titleString,
			footer: {
				icon_url: "https://www.aideron.org/img/logo.png",
				text: "Aideron Robotics"
			}
		},
	});
	//TRACE(JSON.stringify(notificationData) + "\n\n");
}

async function sendPOCONotification(pingChannel, type, text, timestamp) {
	var arr = text.split("\n");
	var notificationData = {};
	notificationData.shield = 100;
	notificationData.armor = 100;
	notificationData.hull = 100;
	for (var j=0;j<arr.length;j++) {
		var arr2 = arr[j].split(": ");
		switch(arr2[0]) {
			case "shieldLevel":
				notificationData.shield = arr2[1]*100;
				break;
			case "armorLevel":
				notificationData.armor = arr2[1]*100;
				break;
			case "hullLevel":
				notificationData.hull = arr2[1]*100;
				break;
			case "aggressorID":
				notificationData.aggressor = arr2[1];
				break;
			case "aggressorCorpID":
				notificationData.corp = arr2[1];
				break;
			case "aggressorAllianceID":
				notificationData.alliance = (arr2[1] == undefined ? "-" : arr2[1]);
				break;
			case "planetID":
				notificationData.planetID = arr2[1];
				var checkExist = "select * from eveonline_eveplanets where planet_id="+notificationData.planetID+";";
				var planetResult = (await sql.query(checkExist));
				if(planetResult.length > 0) {
					notificationData.planetName = planetResult[0].name;
				} else {
					notificationData.planetName = await pullPlanet(notificationData.planetID);
				}
				break;
			case "typeID":
				notificationData.typeID = arr2[1];
				break;
		}
	}
	var msgString="POCO at "+notificationData.planetName+" is under attack!\n\n"+
					"Aggressor          : [zKill Link](https://zkillboard.com/character/"+notificationData.aggressor+"/)\n"+
					//"Aggressor Corp     : "+notificationData.corp+"\n"+
					//"Aggressor Alliance : "+notificationData.alliance+"\n"+
					"\nShield : "+Math.floor(notificationData.shield)+"%\n"+
					"Armor  : "+Math.floor(notificationData.armor)+"%\n"+
					"Hull   : "+Math.floor(notificationData.hull)+"%";

	var pingStr = "@here";
	//var pingStr = "";
	discord.bot.sendMessage({
		to: pingChannel,
		message: pingStr, // You can also send a message with the embed.
		embed: {
			description: msgString,
			color: 53424,
			timestamp: moment(timestamp).format(),
			thumbnail: {
				url: "https://imageserver.eveonline.com/Render/"+notificationData.typeID+"_128.png"
			},
			title: ("POCO Under Attack"),
			footer: {
				icon_url: "https://www.aideron.org/img/logo.png",
				text: "Aideron Robotics"
			}
		},
	});
	//TRACE(JSON.stringify(notificationData) + "\n\n");
}

async function pullPlanet(planetID) {
	TRACE("pullPlanet()+ "+ planetID);
	var planet = JSON.parse(await req.get({url: globe.esiUrl+"/latest/universe/planets/"+ planetID +"/?datasource=tranquility"}));
	TRACE(JSON.stringify(planet) +"\n\n");
	var insertQuery = "INSERT INTO eveonline_eveplanets (planet_id";
	var values = [planetID];
	for(const key in planet) {
		switch (key) {
			case 'planet_id':
			case 'position':
				continue;
				break;
			default:
				insertQuery += ", "+key;
				values.push(planet[key]);
				break;
		}
	}
	insertQuery += ") VALUES (?)";
	var result = (await sql.query(insertQuery, [values]));
	return planet["name"];
}

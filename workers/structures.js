const moment = require('moment');
const globe = require('../modules/globals.js');
const sql = require('../modules/sql-handler.js');
const discord = require('../modules/discord-handler.js');
const req = require('../modules/request-handler.js');

const trace = require('../modules/trace.js');
var TRACE = trace.TRACE;
var firstConnect = true;

discord.bot.on('ready', async function() {
	if (firstConnect) {

		await sql.query("CREATE TABLE IF NOT EXISTS eveonline_evecorpstructures (structure_id bigint(20) NOT NULL,type_id int(11) NOT NULL, corporation_id int(11) NOT NULL, fuel_expires datetime DEFAULT NULL, state tinytext NOT NULL, state_timer_start datetime DEFAULT NULL, state_timer_end datetime DEFAULT NULL, services text, next_reinforce_apply datetime DEFAULT NULL, next_reinforce_hour int(11) DEFAULT NULL, next_reinforce_weekday int(11) DEFAULT NULL, profile_id int(11) NOT NULL, reinforce_hour int(11) NOT NULL, reinforce_weekday int(11) DEFAULT NULL, system_id int(11) NOT NULL, unanchors_at datetime DEFAULT NULL, PRIMARY KEY (structure_id), UNIQUE KEY structure_id (structure_id), KEY structure_id_2 (structure_id))");

		await sql.query("CREATE TABLE IF NOT EXISTS eveonline_evestructures (structure_id bigint(20) NOT NULL, name tinytext NOT NULL, owner_id bigint(20) NOT NULL, solar_system_id bigint(20) NOT NULL, type_id bigint(20) NOT NULL, PRIMARY KEY (structure_id), UNIQUE KEY structure_id (structure_id), KEY structure_id_2 (structure_id))");

		await pullNewCorpStructureData();
		setInterval(pullNewCorpStructureData, 60000);

		//await pullCorpStructureData();
		setInterval(pullCorpStructureData, 3600000);

		//await checkStructureFuel();
		setInterval(checkStructureFuel, 28800000);

		firstConnect = false;
	}
});

module.exports = {}

// Intervaled
async function checkStructureFuel() {
	TRACE("checkStructureFuel()+");
	lowFuelStructures = (await sql.query( "SELECT SEC_TO_TIME(TIMESTAMPDIFF(SECOND, NOW(), fuel_expires)) as fuel_remaining, eveonline_evestructures.name, eveonline_evecorpstructures.* FROM `eveonline_evecorpstructures` left join eveonline_evestructures on eveonline_evecorpstructures.structure_id = eveonline_evestructures.structure_id where DATEDIFF(fuel_expires, NOW())<=3 and TIMESTAMPDIFF(SECOND, NOW(), fuel_expires)>0" ));
	for (var i = 0; i < lowFuelStructures.length; i++) {
		discord.bot.sendMessage({
			to: discord.pingChannel,
			message: '', // You can also send a message with the embed.
			embed: {
				description: lowFuelStructures[i]['name']+"\n\nTime Remaining: "+lowFuelStructures[i]['fuel_remaining'],
				color: 53424,
				timestamp: moment().format(),
				thumbnail: {
					url: "https://imageserver.eveonline.com/Render/"+lowFuelStructures[i]['type_id']+"_128.png"
				},
				title: ("Low Fuel"),
				footer: {
					icon_url: "https://www.aideron.org/img/logo.png",
					text: "Aideron Robotics"
				}
			},
		});
	}
}

// Intervaled
async function pullCorpStructureData() {
	TRACE("pullCorpStructureData()+");
	cfoStrucTokens = (await sql.query("select * from tokens where Scopes=\"esi-universe.read_structures.v1\""));
	var cfoStrucToken = await req.refreshTokenIfNeeded(cfoStrucTokens[0]);

	structureTokens = (await sql.query("select * from tokens where Scopes=\"esi-corporations.read_structures.v1\";"));
	await pullStructureData(cfoStrucToken,structureTokens);
}

// Intervaled
async function pullNewCorpStructureData() {
	TRACE("pullNewCorpStructureData()+");
	cfoStrucTokens = (await sql.query("select * from tokens where Scopes=\"esi-universe.read_structures.v1\""));
	var cfoStrucToken = await req.refreshTokenIfNeeded(cfoStrucTokens[0]);

	structureTokens = (await sql.query("select * from tokens where Scopes=\"esi-corporations.read_structures.v1\" and isNew = TRUE;"));
	await pullStructureData(cfoStrucToken,structureTokens);
}

async function pullStructureData(cfoStrucToken,structureTokens) {
	if (typeof structureTokens == 'undefined' ) return;
	for (var i = 0; i < structureTokens.length; i++) {
		var usableStructureToken = await req.refreshTokenIfNeeded(structureTokens[i]);
		//TRACE(JSON.stringify(usableStructureToken));
		//TRACE(usableStructureToken.CorporationID);
		var resp = JSON.parse(await req.get({token:usableStructureToken.Token, url: globe.esiUrl+"/latest/corporations/"+ usableStructureToken.CorporationID +"/structures/?datasource=tranquility"}));
		if (typeof resp == 'undefined') continue;

		await sql.query("DELETE FROM eveonline_evecorpstructures WHERE corporation_id="+usableStructureToken.CorporationID);

		for (var j = 0; j < resp.length; j++) {
			//TRACE(JSON.stringify(resp[j]));
			var checkExist = "select * from eveonline_evecorpstructures where structure_id="+resp[j]['structure_id'];
			var exist = (await sql.query(checkExist)).length > 0;

			await globe.fetchStructureInfoIfNeeded(resp[j]['structure_id'], cfoStrucToken.Token, true);

			if(!exist) {
				//Insert
				var insertQuery = "INSERT INTO eveonline_evecorpstructures (";
				var values = [];
				for(const key in resp[j]) {
					insertQuery += key+", ";

					switch (key) {
						case 'state_timer_start':
						case 'state_timer_end':
						case 'fuel_expires':
						case 'next_reinforce_apply':
						case 'unanchors_at':
							values.push(globe.sqlFormatDateTime(resp[j][key]) );
							break;
						case 'services':
							values.push(JSON.stringify(resp[j][key]));
							break;
						default:
							values.push(resp[j][key]);
							break;
					}
				}
				insertQuery=insertQuery.slice(0, -2);
				insertQuery +=") VALUES (?)";
				TRACE(insertQuery);
				var result = (await sql.query(insertQuery, [values]));
				TRACE(JSON.stringify(result));
			} else {
				//Update
				var updateQuery = "UPDATE eveonline_evecorpstructures SET ";
				var values = [];
				if (!("services" in  resp[j])) resp[j]["services"]="[]";
				for (const key in resp[j]) {
					if (key == 'structure_id') continue;

					updateQuery += key + "=? , ";

					switch (key) {
						case 'state_timer_start':
						case 'state_timer_end':
						case 'fuel_expires':
						case 'next_reinforce_apply':
						case 'unanchors_at':
							values.push(globe.sqlFormatDateTime(resp[j][key]) );
							break;
						case 'services':
							values.push(JSON.stringify(resp[j][key]));
							break;
						default:
							values.push(resp[j][key]);
							break;
					}
				}
				if (!("state_timer_start" in  resp[j])) updateQuery += "state_timer_start=NULL, ";
				if (!("state_timer_end" in  resp[j])) updateQuery += "state_timer_end=NULL, ";
				if (!("fuel_expires" in  resp[j])) updateQuery += "fuel_expires=NULL, ";
				if (!("unanchors_at" in  resp[j])) updateQuery += "unanchors_at=NULL, ";

				updateQuery = updateQuery.slice(0, -2);
				updateQuery += " WHERE structure_id="+resp[j]['structure_id'];
				TRACE(updateQuery);
				var result = (await sql.query(updateQuery, values));
				TRACE(JSON.stringify(result));
			}
		}
		sql.setTokenUsed(usableStructureToken);
	}
}

var moment = require('moment');

// Neither of the 2 below are used?
var dateFormat = require('dateformat');
const https = require('https');

const globe = require('./modules/globals.js');
const sql = require('./modules/sql-handler.js');
const discord = require('./modules/discord-handler.js');
const req = require('./modules/request-handler.js');

// The files below automatically add their listeners to the bot when they are required. No need to initialize anything.
const notifications = require('./workers/notifications.js');
const structures = require('./workers/structures.js');

// Initialize Discord Bot
var bot = discord.bot;

const trace = require('./modules/trace.js');
var TRACE = trace.TRACE;

bot.on('ready', async function() {
    TRACE('Discord Connected');
    TRACE('Logged in as: ');
    TRACE(bot.username + ' - (' + bot.id + ')');
});

bot.on("debug", function(msg) {
	//TRACE(msg);
});
